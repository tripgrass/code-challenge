<?php 
/*
Template Name: Airport Map
*/
get_header(); ?>
<div class="loading">

	<div id="airport_map">
		
	</div>
	<div class="overlay">
		<div class="wonderful-nav">
			<a href="/">Airport Visualizer</a>
		</div>
		<div class="loading-message">
			<h2>We're loading your airports now, we'll have your map ready in just a minute.</h2>
			<div class="img-container">
				<img src="<?php echo get_template_directory_uri(); ?>/dest/image/loading2.gif">
			</div>
			<div class="trivia">For the first half of 2020, worldwide airport passenger numbers decreased by -58.4% compared to the same period in 2019, with international passenger traffic hit the hardest, recording a -64.5% drop.</div>
		</div>
		<div class="center-content">
			<div class="landing-content">
				<h1><?php echo get_field('mapPage_title'); ?></h1>
				<div class="content"><?php echo get_field('mapPage_content'); ?></div>
			</div>
			<?php echo gravity_form(1, false, false, false, '', true, 12); ?>
		</div>		
	</div>
</div>
<?php get_footer(); ?>