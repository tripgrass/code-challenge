jQuery(document).ready(function($) {
  $('#gform_browse_button_1_1').html('Select File');
  $('.gform_drop_instructions').html('Drag and drop a CSV file, or select one from your computer.');
});
gform.addFilter( 'gform_file_upload_status_markup', function( statusMarkup, file, size, strings, removeFileJs, up ) {
    // Customize statusMarkup as desired.
    //console.log('statusmakrup:::' + statusMarkup);
    statusMarkup = "";
 
    return statusMarkup;
} );
function getLoadingMesage(){
  var messages = [
    "The number of commercial air carriers in the United States has been steadily decreasing, with the number of major carriers falling from 22 in 2008 to 18 in 2020. ",
    "For the first half of 2020, worldwide airport passenger numbers decreased by -58.4% compared to the same period in 2019, with international passenger traffic hit the hardest, recording a -64.5% drop.",
    "In 2019, airport traffic in emerging markets and developing economies grew slightly faster (+3.9%) than in advanced economies (+3.1%). Africa (+6.0%) and Latin America-Caribbean (+4.4%) posted strong growth of all regions in passenger traffic.",
    "In 2020, due to the coronavirus pandemic, the number of scheduled passengers boarded by the global airline industry dropped to just over 1.7 billion people. This represents a 61 percent loss in global air passenger traffic."
  ];
  var message = messages[Math.floor(Math.random() * messages.length)];
  return "Did you know..." + message;
}
gform.addFilter( 'gform_file_upload_markup', function( html, file, up, strings, imagesUrl, response ) {
    var message = getLoadingMesage();
    $('.loading-message .trivia').html(message);
    $('.loading-message').show();
    $('.center-content').css('visibility','hidden');
    setTimeout(function() { 
      $('#gform_1').submit();
    }, 1000);
    return html + "uploaded";
} );
function initMap() {
  window.infoWindows = [];
  window.map = new google.maps.Map(document.getElementById("airport_map"), {
    center: { lat: 37.8283, lng: -95.5795 },
    zoom: 5,
    maxZoom: 22,
    minZoom: 4,
    mapId: '6c7d7eeb7769a2ca',
    disableDefaultUI: true,
  });
  window.map.setOptions({ maxZoom: 15 });
  google.maps.event.addListenerOnce(map, 'idle', function(){
    $('body').addClass('map-loaded');
      // do something only the first time the map is loaded
  });  
}
