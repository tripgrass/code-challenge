<?php get_header(); ?>
<div class="active-map" data-id="<?php echo get_the_ID(); ?>">
	<div id="airport_map"></div>
	<div class="single-overlay overlay">
		<div class="wonderful-nav">
			<a href="/">Airport Visualizer</a>
		</div>

		<div class="center-content">
		</div>		
	</div>
</div>
<?php get_footer(); ?>