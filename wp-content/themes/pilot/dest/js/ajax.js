$(document).ready(function($) {
  if( $('.single .active-map').length ){
    loadMore( $('.active-map').data('id') );
  }
})
function addMarker( airport ){
  const latLong = new google.maps.LatLng(parseFloat(airport[6]), parseFloat(airport[7]));
  const marker = new google.maps.Marker({
    position: latLong,
    map,
    title: airport[1],
    icon: 'https://devthinkjoinery.com/wp-content/themes/pilot/dest/image/marker.png'
  });

  const infowindow = new google.maps.InfoWindow({
    content: `<div class="wonderful-info-window"><div class="airport-name">` + airport[1] + `</div>
                <div class="lat-long">` + airport[6] + `, ` + airport[7] + `</div></div>`,
  });
  window.infoWindows.push( infowindow );
  marker.addListener("click", () => {
    $.each(window.infoWindows,function(k,v){
      v.close();
    })
    infowindow.open({
      anchor: marker,
      map,
      shouldFocus: false,
    });
  });
}
function loadMore( post_id, num_of_airports = 9000 ){
  $.ajax({
    url: ajax.ajaxurl,
    type: 'post',
    async:    true,
    cache:    false,
    dataType: 'json',     
    data: {
      action: 'ajax_airport_map',
      params: {
        'paged': '1',
        'post_id' : post_id,
        'post_types' : 'submission'
      }
    },
    fail: function(result){
      console.log('fail', result);

    },
    success: function( result ) {
      $('.loading-message').hide();
      $('body').addClass('active-map');      
      if( result.airports ){
        $('.center-content').css('visibility','visible');
        var i = 0,
          panned = 0,
           total_num_of_airports = Object.keys(result.airports).length;
        $.each(result.airports, function(k,airport){
          if( i > num_of_airports){
            return false;
          }
          if( i < 10 ){

            if(airport[6] && airport[7]){
              addMarker( airport );
            }
            console.log(i);
            if( i == 9){
              console.log('panning');
              const latLong = new google.maps.LatLng(parseFloat(airport[6]), parseFloat(airport[7]));
              map.panTo(latLong);
              panned = 1;
            }
          }
          if( i >= 10 ){
            if(airport[6] && airport[7]){
              addMarker( airport );
            }            
          }
          if( (0 == panned) && i == (total_num_of_airports - 1) || i == num_of_airports ){
            const latLong = new google.maps.LatLng(parseFloat(airport[6]), parseFloat(airport[7]));
            map.panTo(latLong);
          }
          i++;
        })
      }
      $.ajax({
        url: ajax.ajaxurl,
        type: 'post',
        async:    true,
        cache:    false,
        dataType: 'json',     
        data: {
          action: 'ajax_create_airport_meta',
          params: {
            'post_id' : post_id
          }
        },
        fail: function(result){
          console.log('fail create meta', result);
        },
        success: function( result ) {
          console.log('success;;;;;',result);
        }
      })
    }
  })    
}
function copyUrl(){
  var url = $('.copy-button').html();
  textArea = document.createElement("textarea");
  textArea.value = url;
  textArea.style.position="fixed"; 
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();
  document.execCommand('copy');
  $('.copy-button').addClass('copied');
  setTimeout(function() { 
    $('.copy-button').removeClass('copied');
  }, 2000);
}
