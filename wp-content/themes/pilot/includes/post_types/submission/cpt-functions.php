<?php
	require get_template_directory() . '/includes/post_types/submission/cpt-def.php';
	require get_template_directory() . '/includes/post_types/submission/cpt-acf.php';
	require get_template_directory() . '/includes/post_types/submission/submission.class.php';

function get_all_submissions(){
	$submissions = array();
	$args=array(
	  'post_type' => 'submission',
	  'post_status' => 'publish',
	  'posts_per_page' => -1
	);
	$loc_query = new WP_Query($args);
	if( $loc_query->have_posts() ):
		foreach($loc_query->posts as $loc_post):
			$submissions[] = new submission($loc_post);			
		endforeach;
	endif;
	return $submissions;
}
function pilot_submission( $submission_ref = null ){
	$submission = new Pilot\submission( $submission_ref );
	if( $submission->is_admin() ){
		return new Pilot\Admin( $submission );
	}
	return $submission;

}