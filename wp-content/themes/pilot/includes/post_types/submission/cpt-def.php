<?php
/**
 * Submission Custom Post Type
 */
add_action( 'init', 'register_submission_post_type');
function register_submission_post_type()
{ 
	register_post_type( 'submission',
		array( 'labels' => 
			array(
				'name'               => 'Submissions',
				'singular_name'      => 'Submission',
				'all_items'          => 'All Submissions',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Submission',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Submission',
				'new_item'           => 'New Submission',
				'view_item'          => 'View Submission',
				'search_items'       => 'Search Submissions',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Submissions post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-location-alt',
			 'rewrite'	      => array( 'slug' => 'submission', 'with_front' => false ),
			 'has_archive'      => false,
			'capability_type'     => 'page',
			'supports'            => array( 'title','author','thumbnail')
		)
	);
}
?>