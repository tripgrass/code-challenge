<?php
class PilotSubmission{
	public function __construct( $submission_ref = null ){
		global $pilot;
		if( is_numeric( $submission_ref ) ){
			$submission = get_user_by( 'ID', $submission_ref );
		}
		elseif( is_object( $submission_ref ) ){
			$submission = $submission_ref;
		}
		if( is_object( $submission ) ){
			foreach( $submission as $key => $value ){
				$this->$key = $value;
			}
		}
		$this->get_submission_meta();
		$this->wp_submission = $submission;
	}
	public function get_submission_meta(){
		$this->permalink = get_permalink($this->ID);

		$thumb_id = get_post_thumbnail_id($this->ID);
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
		$this->thumbnail = $thumb_url_array[0];

		$this->get_services();
		$this->get_address();
	}
	public function get_address(){
		$address_array = get_field('address',$this->ID);
		if(is_array($address_array)){
			$this->address = $address_array['address'];
			$this->lat = $address_array['lat'];
			$this->lng = $address_array['lng'];
		}
	}
	public function get_services(){
		$services = get_posts(array(
			'post_type' => 'service',
			'meta_query' => array(
				array(
					'key' => 'service_submissions', // name of custom field
					'value' => '"' . $this->ID . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			)
		));
		$this->services = [];
		foreach( $services as $service){
				$array = [
					'ID' => $service->ID,
					'title' => $service->post_title,
					'slug' => $service->post_name
				];
				$this->services[] = $array;
		}
	}

}