<?php
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	
	function asset_path($filename) {
		$dist_path = get_template_directory_uri() . DIST_DIR;
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;
		
		if (empty($manifest)) {
			$manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get().array($file);
		} else {
			return $dist_path . $directory . $file;
		}
	}

	// Custom styling for Login Page

	function login_styles() {
	?>
		<style type="text/css"> 
			body.login div#login h1 a {
				background-image: url( <?php echo site_url(); ?>/wp-content/themes/pilot/image/Logo.svg);
				background-size: 140px auto;
				width: 140px;
			}
		</style>
	<?php
	} 
	add_action( 'login_enqueue_scripts', 'login_styles' );
add_action( 'wp_head', 'wpse33072_wp_head', 1 );
/**
 * Remove feed links from wp_head
 */
function wpse33072_wp_head()
{
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
}
add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );
function acf_load_mapPage_upload_form_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

	$forms = GFAPI::get_forms();

    // loop through array and add to field 'choices'
    if( is_array($forms) ) {
        
        foreach( $forms as $form ) {
            
            $field['choices'][ $form['id'] ] = $form['title'];
            
        }
        
    }
    

    // return the field
    return $field;
    
}

add_filter('acf/load_field/name=mapPage_upload_form', 'acf_load_mapPage_upload_form_field_choices');
add_filter( 'gform_confirmation', 'custom_confirmation_message', 10, 4 );
function custom_confirmation_message( $confirmation, $form, $entry, $ajax ) {
//add_action( 'gform_after_submission', 'set_post_content', 10, 2 );
//function set_post_content( $entry, $form) {
	// get mapPage form::
	set_time_limit(3600);
	error_log(print_r($entry,true));
	$form_field = get_field('mapPage_upload_form');
	if( $form_field && $form_field == $form['id'] ){
		$csv_files = json_decode($entry[1]);
		$csv_file = $csv_files[0];
		preg_match('#wp-content#', $csv_file, $csv_matches, PREG_OFFSET_CAPTURE); 
		$new_file = substr($csv_file, ($csv_matches[0][1] ));

		$root = get_template_directory();
		preg_match('#wp-content#', $root, $root_matches, PREG_OFFSET_CAPTURE); 
		$remove = "#" . substr($root, $root_matches[0][1] ) ."#";
		$new_root = preg_replace($remove, "", $root);

		$csv_location = $new_root . $new_file;
		$handle = fopen($csv_location, "r");
		if (($handle) !== FALSE) {
			$title = date('m/d/Y h:i:s a', time() );
			$post_id = wp_insert_post(array (
			    'post_type' => 'submission',
			    'post_title' => $title,
			    'post_name' => strtotime($title),
			    'post_status' => 'publish',
			    'comment_status' => 'closed',
			    'ping_status' => 'closed'
			));
			$current_csv_row = 0;
			$csv_array = array_map('str_getcsv', file($csv_location));
			if( 'ID' == $csv_array[0][0] ){
				unset($csv_array[0] );
			}
			if ($post_id) {
				$airport_meta = [];
				update_field('submission_gforms_file', $csv_file, $post_id);
				update_field('submission_current_csv_row', $current_csv_row, $post_id);
				update_field('submission_csv_array', $csv_array, $post_id);
			}
			fclose($handle);
		}		
	}
	else{
		error_log(print_r($form,true));		
	}
	$url = site_url() . "/" . strtotime($title);
    $confirmation .= "<script>window.top.jQuery(document).on('gform_confirmation_loaded', function () { 
    	$('.gform_confirmation_wrapper').html(`<div class='wonderful-confirmation'><h2>Nice Airports!</h2>  Your shareable url is:  <button class='copy-button' onclick='copyUrl()'>" . $url . "</button>`);
    	console.log('gform_confirmation_loaded running'); } );    	  
    	loadMore( " . $post_id . ");

</script>";
    return $confirmation;
}
add_action( 'wp_enqueue_scripts', 'ajax_blog_enqueue' );
function ajax_blog_enqueue() {
	wp_enqueue_script( 'ajax',  get_stylesheet_directory_uri() . '/dest/js/ajax.js', array( 'jquery' ), '1.0', true );
	wp_localize_script( 'ajax', 'ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
}

add_action( 'wp_ajax_nopriv_ajax_airport_map', 'ajax_airport_map' );
add_action( 'wp_ajax_ajax_airport_map', 'ajax_airport_map' );
function ajax_airport_map() {
	$params = $_REQUEST['params'];
	$post_id = $params['post_id'];
	$post = get_post($post_id);
	$post->airports = get_field('submission_csv_array',$post_id);
	echo json_encode( $post);
	exit;
}	
add_action( 'wp_ajax_nopriv_ajax_create_airport_meta', 'ajax_create_airport_meta' );
add_action( 'wp_ajax_ajax_create_airport_meta', 'ajax_create_airport_meta' );
function ajax_create_airport_meta() {
	set_time_limit(3600);

	$params = $_REQUEST['params'];
	$post_id = $params['post_id'];
	$post = get_post($post_id);
	$airports = get_field('submission_csv_array',$post_id);
	$current_csv_row = get_field('submission_current_csv_row', $post_id);

	error_log('here:::1888');
	error_log('here:::$current_csv_row' . $current_csv_row);
	error_log( 'last_key' . $last_key);
	$last_key = (count($airports)  );
	if( $current_csv_row < $last_key ){
		if (is_array($airports) ) {
			error_log('here:::192' );
			$airport_meta = [];
			update_field('submission_current_csv_row', $current_csv_row, $post_id);
			while( $current_csv_row <= $last_key ) {
			error_log('here:::196' . $current_csv_row );
				$current_row = $airports[$current_csv_row];
				$airport_meta = array(
					"airport_id"   => esc_sql($current_row[0]),
					"airport_name"   => esc_sql($current_row[1]),
					"airport_city"   => esc_sql($current_row[2]),
					"airport_country"   => esc_sql($current_row[3]),
					"airport_iatafaa"   => esc_sql($current_row[4]),
					"airport_icao"   => esc_sql($current_row[5]),
					"airport_latitude"   => esc_sql($current_row[6]),
					"airport_longitude"   => esc_sql($current_row[7]),
					"airport_altitude"   => esc_sql($current_row[8]),
					"airport_timezone"   => esc_sql($current_row[9])
				);
				add_row('submission_airports', $airport_meta, $post_id);
				$current_csv_row++;
				update_field('submission_current_csv_row', $current_csv_row, $post_id);
			}	
	//		error_log(print_r($airport_meta,true));		
		}
	}
	echo json_encode( ( $last_key - $current_csv_row ) );
	exit;
}	





?>